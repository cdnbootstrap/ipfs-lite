package threads.server.fragments;


import android.app.Activity;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;
import androidx.work.WorkManager;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.navigationrail.NavigationRailView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.DeleteOperation;
import threads.server.core.events.EVENTS;
import threads.server.core.threads.SortOrder;
import threads.server.core.threads.THREADS;
import threads.server.core.threads.Thread;
import threads.server.core.threads.ThreadsViewModel;
import threads.server.provider.FileProvider;
import threads.server.services.LiteService;
import threads.server.services.MimeTypeService;
import threads.server.services.QRCodeService;
import threads.server.utils.Folder;
import threads.server.utils.SelectionViewModel;
import threads.server.utils.ThreadItemDetailsLookup;
import threads.server.utils.ThreadsItemKeyProvider;
import threads.server.utils.ThreadsViewAdapter;
import threads.server.work.CopyDirectoryWorker;
import threads.server.work.CopyFileWorker;
import threads.server.work.PageRefreshWorker;
import threads.server.work.UploadFolderWorker;


public class ThreadsFragment extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener, ThreadsViewAdapter.ThreadsViewAdapterListener {

    private static final String TAG = ThreadsFragment.class.getSimpleName();


    private static final int CLICK_OFFSET = 500;
    @NonNull
    private final AtomicReference<LiveData<List<Thread>>> mObserver = new AtomicReference<>(null);


    private final ActivityResultLauncher<Intent> mFolderImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                long parent = getThread(requireContext());
                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    int items = mClipData.getItemCount();
                                    if (items > 0) {
                                        for (int i = 0; i < items; i++) {
                                            ClipData.Item item = mClipData.getItemAt(i);
                                            Uri uri = item.getUri();

                                            if (!FileProvider.hasReadPermission(requireContext(), uri)) {
                                                EVENTS.getInstance(requireContext()).error(
                                                        getString(R.string.file_has_no_read_permission));
                                                return;
                                            }

                                            if (FileProvider.isPartial(requireContext(), uri)) {
                                                EVENTS.getInstance(requireContext()).error(
                                                        getString(R.string.file_not_valid));
                                                return;
                                            }

                                            UploadFolderWorker.load(requireContext(), parent, uri);
                                        }
                                    }
                                } else {
                                    Uri uri = data.getData();
                                    if (uri != null) {
                                        if (!FileProvider.hasReadPermission(requireContext(), uri)) {
                                            EVENTS.getInstance(requireContext()).error(
                                                    getString(R.string.file_has_no_read_permission));
                                            return;
                                        }

                                        if (FileProvider.isPartial(requireContext(), uri)) {
                                            EVENTS.getInstance(requireContext()).error(
                                                    getString(R.string.file_not_valid));
                                            return;
                                        }

                                        UploadFolderWorker.load(requireContext(), parent, uri);
                                    }
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mFilesImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);

                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    long parent = getThread(requireContext());
                                    LiteService.files(requireContext(), mClipData, parent);

                                } else if (data.getData() != null) {
                                    Uri uri = data.getData();
                                    Objects.requireNonNull(uri);
                                    if (!FileProvider.hasReadPermission(requireContext(), uri)) {
                                        EVENTS.getInstance(requireContext()).error(
                                                getString(R.string.file_has_no_read_permission));
                                        return;
                                    }

                                    if (FileProvider.isPartial(requireContext(), uri)) {
                                        EVENTS.getInstance(requireContext()).error(
                                                getString(R.string.file_not_valid));
                                        return;
                                    }

                                    long parent = getThread(requireContext());

                                    LiteService.file(requireContext(), parent, uri);
                                }

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mDirExportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                Uri uri = data.getData();
                                Objects.requireNonNull(uri);

                                if (!FileProvider.hasWritePermission(requireContext(), uri)) {
                                    EVENTS.getInstance(requireContext()).error(
                                            getString(R.string.file_has_no_write_permission));
                                    return;
                                }
                                long threadIdx = getThread(requireContext());
                                CopyDirectoryWorker.copyTo(requireContext(), uri, threadIdx);

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mFileExportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                Uri uri = data.getData();
                                Objects.requireNonNull(uri);
                                if (!FileProvider.hasWritePermission(requireContext(), uri)) {
                                    EVENTS.getInstance(requireContext()).error(
                                            getString(R.string.file_has_no_write_permission));
                                    return;
                                }
                                long threadIdx = getThread(requireContext());
                                CopyFileWorker.copyTo(requireContext(), uri, threadIdx);

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private boolean widthMode = false;
    private SelectionViewModel mSelectionViewModel;
    private ThreadsViewAdapter mThreadsViewAdapter;
    private ThreadsViewModel mThreadsViewModel;
    private long mLastClickTime = 0;
    private RecyclerView mRecyclerView;
    private ActionMode mActionMode;
    private SelectionTracker<Long> mSelectionTracker;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ThreadItemDetailsLookup mThreadItemDetailsLookup;
    private NavigationRailView mNavigationRailView;
    private ExtendedFloatingActionButton mFloatingActionButton;

    private static long getThread(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                TAG, Context.MODE_PRIVATE);
        return sharedPref.getLong(Content.IDX, -1);
    }

    private static void setThread(@NonNull Context context, long idx) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(Content.IDX, idx);
        editor.apply();
    }

    public static String left(String str, final int len) {
        if (str == null) {
            return "";
        }
        if (len < 0) {
            return "";
        }
        str = str.trim();
        if (str.length() <= len) {
            return str;
        }
        return str.substring(0, len).concat("...");
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        releaseActionMode();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mSelectionTracker != null) {
            mSelectionTracker.onSaveInstanceState(outState);
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void findInPage() {
        try {
            if (isResumed()) {
                mActionMode = ((AppCompatActivity)
                        requireActivity()).startSupportActionMode(
                        createSearchActionModeCallback());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void createFolderChips(@NonNull ChipGroup group, @NonNull List<Folder> folders) {

        for (int i = folders.size(); i < group.getChildCount(); i++) {
            group.removeViewAt(i);
        }

        int size = folders.size();
        for (int i = 0; i < size; i++) {
            Folder folder = folders.get(i);

            Chip mChip = (Chip) group.getChildAt(i);

            if (mChip == null) {

                mChip = (Chip) getLayoutInflater().inflate(R.layout.item_chip_folder,
                        group, false);
                group.addView(mChip);
            }

            mChip.setText(folder.getName());

            mChip.setOnCheckedChangeListener((compoundButton, b) -> {

                mSelectionTracker.clearSelection();
                mSelectionViewModel.setParentThread(folder.getIdx());
            });

        }

    }

    private void createFolders(long thread) {
        THREADS threads = THREADS.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            List<Folder> folders = new ArrayList<>();
            List<Thread> ancestors = threads.getAncestors(thread);
            for (Thread ancestor : ancestors) {

                String name = left(ancestor.getName(), 20);

                folders.add(new Folder(name, ancestor.getIdx()));
            }
            mSelectionViewModel.setFolders(folders);
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.threads_view, container, false);
    }

    private void switchDisplayModes() {
        if (!widthMode) {
            mNavigationRailView.setVisibility(View.GONE);
        } else {
            mNavigationRailView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            evaluateDisplayModes();
            switchDisplayModes();
            showFab(Objects.requireNonNull(mSelectionViewModel.getShowFab().getValue()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void clickFilesAdd() {

        try {
            Long idx = mSelectionViewModel.getParentThread().getValue();
            Objects.requireNonNull(idx);

            setThread(requireContext(), idx);

            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType(MimeTypeService.ALL);
            String[] mimeTypes = {MimeTypeService.ALL};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            mFilesImportForResult.launch(intent);

        } catch (Throwable e) {
            EVENTS.getInstance(requireContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
            LogUtils.error(TAG, e);
        }
    }

    private void showFab(boolean showFab) {
        if (widthMode) {
            mFloatingActionButton.hide();
        } else {
            if (showFab) {
                mFloatingActionButton.show();
            } else {
                mFloatingActionButton.hide();
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mFloatingActionButton = view.findViewById(R.id.floating_action_button);


        mFloatingActionButton.setOnClickListener((v) -> {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            clickFilesAdd();

        });


        mNavigationRailView = view.findViewById(R.id.navigation_rail);
        switchDisplayModes();


        mNavigationRailView.setOnItemSelectedListener(item -> {
            int id = item.getItemId();
            if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                return true;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            if (id == R.id.menu_add_file) {
                try {
                    clickFilesAdd();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_new_text) {
                try {
                    long parent = 0L;
                    Long thread = mSelectionViewModel.getParentThread().getValue();
                    if (thread != null) {
                        parent = thread;
                    }

                    TextDialogFragment.newInstance(parent).
                            show(getChildFragmentManager(), TextDialogFragment.TAG);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_import_folder) {
                try {

                    long parent = 0L;
                    Long thread = mSelectionViewModel.getParentThread().getValue();
                    if (thread != null) {
                        parent = thread;
                    }
                    setThread(requireContext(), parent);

                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                    mFolderImportForResult.launch(intent);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_new_folder) {
                try {

                    long parent = 0L;
                    Long thread = mSelectionViewModel.getParentThread().getValue();
                    if (thread != null) {
                        parent = thread;
                    }

                    NewFolderDialogFragment.newInstance(parent).
                            show(getChildFragmentManager(), NewFolderDialogFragment.TAG);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            }
            return false;
        });


        ChipGroup chipGroup = view.findViewById(R.id.folder_group);
        HorizontalScrollView scrollView = view.findViewById(R.id.folder_scroll_view);

        ImageView homeAction = view.findViewById(R.id.home_action);
        homeAction.setOnClickListener((v) -> {

            mSelectionTracker.clearSelection();
            mSelectionViewModel.setParentThread(0L);

        });

        scrollView.setVisibility(View.GONE);


        mSelectionViewModel = new ViewModelProvider(requireActivity()).get(SelectionViewModel.class);

        mSelectionViewModel.getFolders().observe(getViewLifecycleOwner(), (folders) -> {
            try {
                if (folders != null) {
                    createFolderChips(chipGroup, folders);
                } else {
                    createFolderChips(chipGroup, new ArrayList<>());
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mSelectionViewModel.getParentThread().observe(getViewLifecycleOwner(), (thread) -> {

            try {
                if (thread != null) {
                    createFolders(thread);

                    if (thread == 0L) {
                        scrollView.setVisibility(View.GONE);
                    } else {
                        scrollView.setVisibility(View.VISIBLE);
                    }

                    SortOrder sortOrder = mSelectionViewModel.getSortOrder().getValue();
                    String query = mSelectionViewModel.getQuery().getValue();

                    updateDirectory(thread, query, sortOrder, false);


                    scrollView.post(() -> scrollView.scrollTo(chipGroup.getRight(), chipGroup.getTop()));
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mSelectionViewModel.getShowFab().observe(getViewLifecycleOwner(), (showFab) -> {
            try {
                if (showFab != null) {
                    showFab(showFab);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mSelectionViewModel.getQuery().observe(getViewLifecycleOwner(), (query) -> {
            try {
                if (query != null) {
                    Long parent = mSelectionViewModel.getParentThread().getValue();
                    SortOrder sortOrder = mSelectionViewModel.getSortOrder().getValue();
                    updateDirectory(parent, query, sortOrder, false);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mSelectionViewModel.getSortOrder().observe(getViewLifecycleOwner(), (sortOrder) -> {
            try {
                if (sortOrder != null) {
                    Long parent = mSelectionViewModel.getParentThread().getValue();
                    String query = mSelectionViewModel.getQuery().getValue();
                    updateDirectory(parent, query, sortOrder, true);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mThreadsViewModel = new ViewModelProvider(this).get(ThreadsViewModel.class);

        mRecyclerView = view.findViewById(R.id.recycler_view_message_list);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mRecyclerView.setItemAnimator(null);

        mThreadsViewAdapter = new ThreadsViewAdapter(requireContext(), this);
        mRecyclerView.setAdapter(mThreadsViewAdapter);


        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                boolean hasSelection = mSelectionTracker.hasSelection();
                if (dy > 0) {
                    mSelectionViewModel.setShowFab(false);
                } else if (dy < 0 && !hasSelection) {
                    mSelectionViewModel.setShowFab(true);
                }

            }
        });

        mThreadItemDetailsLookup = new ThreadItemDetailsLookup(mRecyclerView);


        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);


        mSelectionTracker = new SelectionTracker.Builder<>(TAG, mRecyclerView,
                new ThreadsItemKeyProvider(mThreadsViewAdapter),
                mThreadItemDetailsLookup,
                StorageStrategy.createLongStorage())
                .build();


        mSelectionTracker.addObserver(new SelectionTracker.SelectionObserver<Long>() {
            @Override
            public void onSelectionChanged() {
                if (!mSelectionTracker.hasSelection()) {
                    if (mActionMode != null) {
                        mActionMode.finish();
                    }
                } else {
                    if (mActionMode == null) {
                        mActionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (mActionMode != null) {
                    mActionMode.setTitle("" + mSelectionTracker.getSelection().size());
                }
                super.onSelectionChanged();
            }

            @Override
            public void onSelectionRestored() {
                if (!mSelectionTracker.hasSelection()) {
                    if (mActionMode != null) {
                        mActionMode.finish();
                    }
                } else {
                    if (mActionMode == null) {
                        mActionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (mActionMode != null) {
                    mActionMode.setTitle("" + mSelectionTracker.getSelection().size());
                }
                super.onSelectionRestored();
            }
        });

        mThreadsViewAdapter.setSelectionTracker(mSelectionTracker);


        if (savedInstanceState != null) {
            mSelectionTracker.onRestoreInstanceState(savedInstanceState);
        } else {
            evaluateDisplayModes();
            switchDisplayModes();
        }

    }


    private void updateDirectory(@Nullable Long parent, @Nullable String query,
                                 @Nullable SortOrder sortOrder, boolean forceScrollToTop) {
        try {

            LiveData<List<Thread>> obs = mObserver.get();
            if (obs != null) {
                obs.removeObservers(getViewLifecycleOwner());
            }

            if (parent == null) {
                parent = 0L;
            }

            if (query == null) {
                query = "";
            }


            LiveData<List<Thread>> liveData = mThreadsViewModel.getLiveDataThreads(parent, query);
            mObserver.set(liveData);


            liveData.observe(getViewLifecycleOwner(), (threads) -> {

                if (threads != null) {
                    if (sortOrder == null) {
                        threads.sort(Comparator.comparing(Thread::getLastModified).reversed());
                    } else {
                        switch (sortOrder) {
                            case DATE: {
                                threads.sort(Comparator.comparing(Thread::getLastModified).reversed());
                                break;
                            }
                            case DATE_INVERSE: {
                                threads.sort(Comparator.comparing(Thread::getLastModified));
                                break;
                            }
                            case SIZE: {
                                threads.sort(Comparator.comparing(Thread::getSize));
                                break;
                            }
                            case SIZE_INVERSE: {
                                threads.sort(Comparator.comparing(Thread::getSize).reversed());
                                break;
                            }
                            case NAME: {
                                threads.sort(Comparator.comparing(Thread::getName));
                                break;
                            }
                            case NAME_INVERSE: {
                                threads.sort(Comparator.comparing(Thread::getName).reversed());
                                break;
                            }
                            default:
                                threads.sort(Comparator.comparing(Thread::getLastModified).reversed());
                        }
                    }

                    int size = mThreadsViewAdapter.getItemCount();
                    boolean scrollToTop = size < threads.size();


                    mThreadsViewAdapter.updateData(threads);

                    if (scrollToTop || forceScrollToTop) {
                        try {
                            mRecyclerView.scrollToPosition(0);
                        } catch (Throwable e) {
                            LogUtils.error(TAG, e);
                        }
                    }
                }
            });
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }


    private long[] convert(Selection<Long> entries) {
        int i = 0;

        long[] basic = new long[entries.size()];
        for (Long entry : entries) {
            basic[i] = entry;
            i++;
        }

        return basic;
    }

    private void deleteAction() {

        final EVENTS events = EVENTS.getInstance(requireContext());

        if (!mSelectionTracker.hasSelection()) {
            events.warning(getString(R.string.no_marked_file_delete));
            return;
        }


        try {
            long[] entries = convert(mSelectionTracker.getSelection());

            removeThreads(entries);

            mSelectionTracker.clearSelection();

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

    }


    private void removeThreads(long... indices) {

        THREADS threads = THREADS.getInstance(requireContext());
        EVENTS events = EVENTS.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();

            try {
                threads.setThreadsDeleting(indices);
                Gson gson = new Gson();
                DeleteOperation deleteOperation = new DeleteOperation();
                deleteOperation.indices = indices;

                String content = gson.toJson(deleteOperation, DeleteOperation.class);
                events.delete(content);

            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }

        });
    }


    @Override
    public void invokeAction(@NonNull Thread thread, @NonNull View view) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        try {
            boolean isSeeding = thread.isSeeding();

            PopupMenu menu = new PopupMenu(requireContext(), view);
            menu.inflate(R.menu.popup_threads_menu);
            menu.getMenu().findItem(R.id.popup_rename).setVisible(true);
            menu.getMenu().findItem(R.id.popup_share).setVisible(true);
            menu.getMenu().findItem(R.id.popup_delete).setVisible(true);
            menu.getMenu().findItem(R.id.popup_copy_to).setVisible(isSeeding);

            menu.setOnMenuItemClickListener((item) -> {

                if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                    return true;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (item.getItemId() == R.id.popup_info) {
                    clickThreadInfo(thread);
                    return true;
                } else if (item.getItemId() == R.id.popup_delete) {
                    clickThreadDelete(thread.getIdx());
                    return true;
                } else if (item.getItemId() == R.id.popup_share) {
                    clickThreadShare(thread.getIdx());
                    return true;
                } else if (item.getItemId() == R.id.popup_copy_to) {
                    clickThreadCopy(thread);
                    return true;
                } else if (item.getItemId() == R.id.popup_rename) {
                    clickThreadRename(thread);
                    return true;
                } else {
                    return false;
                }
            });

            menu.show();


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


    }


    private void clickThreadRename(@NonNull Thread thread) {
        try {
            RenameFileDialogFragment.newInstance(thread.getIdx(), thread.getName()).
                    show(getChildFragmentManager(), RenameFileDialogFragment.TAG);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void clickThreadShare(long idx) {
        EVENTS events = EVENTS.getInstance(requireContext());
        THREADS threads = THREADS.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                Thread thread = threads.getThreadByIdx(idx);
                Objects.requireNonNull(thread);
                ComponentName[] names = {new ComponentName(
                        requireContext().getApplicationContext(), MainActivity.class)};
                Uri uri = DOCS.getInstance(requireContext()).getPath(thread);

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, uri.toString());
                intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                intent.putExtra(Intent.EXTRA_SUBJECT, thread.getName());
                intent.putExtra(Intent.EXTRA_TITLE, thread.getName());


                Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names);
                chooser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(chooser);


            } catch (Throwable ignore) {
                events.warning(getString(R.string.no_activity_found_to_handle_uri));
            }
        });


    }

    private void clickThreadCopy(@NonNull Thread thread) {

        setThread(requireContext(), thread.getIdx());
        try {
            if (thread.isDir()) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                mDirExportForResult.launch(intent);
            } else {
                Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                intent.setType(thread.getMimeType());
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                intent.putExtra(Intent.EXTRA_TITLE, thread.getName());
                intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                mFileExportForResult.launch(intent);
            }
        } catch (Throwable e) {
            EVENTS.getInstance(requireContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
        }

    }

    private ActionMode.Callback createActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_threads_action_mode, menu);

                mSelectionViewModel.setShowFab(false);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {


                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                int itemId = item.getItemId();
                if (itemId == R.id.action_mode_mark_all) {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                        return true;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    mThreadsViewAdapter.selectAllThreads();

                    return true;
                } else if (itemId == R.id.action_mode_delete) {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                        return true;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    deleteAction();

                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

                mSelectionTracker.clearSelection();

                mSelectionViewModel.setShowFab(true);

                if (mActionMode != null) {
                    mActionMode = null;
                }

            }
        };

    }

    @Override
    public void onClick(@NonNull Thread thread) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        try {
            if (!mSelectionTracker.hasSelection()) {

                if (mActionMode != null) {
                    mActionMode.finish();
                    mActionMode = null;
                }

                if (thread.isDir()) {
                    mSelectionViewModel.setParentThread(thread.getIdx());
                } else {
                    clickThreadPlay(thread);
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

    }


    private void clickThreadInfo(@NonNull Thread thread) {
        try {
            String cid = thread.getContent();
            Objects.requireNonNull(cid);
            String uri = Content.IPFS + "://" + cid;

            Uri uriImage = QRCodeService.getImage(requireContext(), uri);
            ContentDialogFragment.newInstance(uriImage,
                    getString(R.string.url_data_access, thread.getName()), uri)
                    .show(getChildFragmentManager(), ContentDialogFragment.TAG);


        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

    }

    private void evaluateDisplayModes() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;
        widthMode = widthDp >= 600;
    }

    private void clickThreadPlay(@NonNull Thread thread) {

        EVENTS events = EVENTS.getInstance(requireContext());
        DOCS docs = DOCS.getInstance(requireContext());
        if (thread.isSeeding()) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(() -> {
                try {
                    String cid = thread.getContent();
                    Objects.requireNonNull(cid);

                    String mimeType = thread.getMimeType();

                    // special case
                    if (Objects.equals(mimeType, MimeTypeService.URL_MIME_TYPE)) {
                        IPFS ipfs = IPFS.getInstance(requireContext());
                        Uri uri = Uri.parse(ipfs.getText(Cid.decode(cid), () -> false));
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        return;
                    } else if (Objects.equals(mimeType, MimeTypeService.HTML_MIME_TYPE)) {
                        Uri uri = docs.getIpnsPath(thread, false);
                        EVENTS.getInstance(requireContext()).uri(uri.toString());
                        return;
                    }
                    Uri uri = docs.getIpnsPath(thread, true);
                    //Uri uri = DOCS.getInstance(mContext).getPath(thread);
                    EVENTS.getInstance(requireContext()).uri(uri.toString());


                } catch (Throwable ignore) {
                    events.warning(getString(R.string.no_activity_found_to_handle_uri));
                }
            });
        }
    }

    @Override
    public void invokePauseAction(@NonNull Thread thread) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();


        UUID uuid = thread.getWorkUUID();
        if (uuid != null) {
            WorkManager.getInstance(requireContext()).cancelWorkById(uuid);
        }

        THREADS threads = THREADS.getInstance(requireContext());
        Executors.newSingleThreadExecutor().submit(() ->
                threads.resetThreadLeaching(thread.getIdx()));

    }


    private int dp48ToPixels() {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) 48 * density);
    }

    private void clickThreadDelete(long idx) {
        removeThreads(idx);
    }


    private ActionMode.Callback createSearchActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_search_action_mode, menu);

                mThreadItemDetailsLookup.setActive(false);

                MenuItem searchMenuItem = menu.findItem(R.id.action_search);

                SearchView mSearchView = (SearchView) searchMenuItem.getActionView();

                TextView textView = mSearchView.findViewById(
                        androidx.appcompat.R.id.search_src_text);
                textView.setTextSize(16);
                textView.setMinHeight(dp48ToPixels());

                mSearchView.setIconifiedByDefault(false);
                mSearchView.setFocusable(true);
                mSearchView.setFocusedByDefault(true);
                String query = mSelectionViewModel.getQuery().getValue();
                Objects.requireNonNull(query);
                mSearchView.setQuery(query, true);
                mSearchView.setIconified(false);
                mSearchView.requestFocus();


                mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {

                        mSelectionViewModel.getQuery().setValue(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        mSelectionViewModel.getQuery().setValue(newText);
                        return false;
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                try {
                    mThreadItemDetailsLookup.setActive(true);
                    mSelectionViewModel.setQuery("");

                    if (mActionMode != null) {
                        mActionMode = null;
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        };

    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);

        try {

            EVENTS.getInstance(requireContext()).warning(getString(R.string.publish_files));

            PageRefreshWorker.publish(requireContext());

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

    public void releaseActionMode() {
        try {
            if (isResumed()) {
                if (mActionMode != null) {
                    mActionMode.finish();
                    mActionMode = null;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

}
