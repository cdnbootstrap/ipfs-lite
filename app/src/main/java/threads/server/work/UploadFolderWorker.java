package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.server.InitApplication;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.threads.THREADS;
import threads.server.services.MimeTypeService;


public class UploadFolderWorker extends Worker {
    private static final String WID = "IFW";
    private static final String TAG = UploadFolderWorker.class.getSimpleName();
    private final NotificationManager mNotificationManager;
    private final THREADS threads;
    private final DOCS docs;
    private final IPFS ipfs;

    @SuppressWarnings("WeakerAccess")
    public UploadFolderWorker(@NonNull Context context,
                              @NonNull WorkerParameters params) {
        super(context, params);
        threads = THREADS.getInstance(context);
        docs = DOCS.getInstance(context);
        ipfs = IPFS.getInstance(context);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    private static OneTimeWorkRequest getWork(long idx, @NonNull Uri uri) {

        Data.Builder data = new Data.Builder();
        data.putString(Content.URI, uri.toString());
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(UploadFolderWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void load(@NonNull Context context, long idx, @NonNull Uri uri) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + uri, ExistingWorkPolicy.KEEP, getWork(idx, uri));

    }

    @Override
    public void onStopped() {
        super.onStopped();
        closeNotification();
    }


    private void closeNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(getId().hashCode());
        }
    }

    private void reportProgress(@NonNull String title, int percent, int index, int maxIndex) {

        if (!isStopped()) {

            Notification notification = createNotification(title, percent, index, maxIndex);

            if (mNotificationManager != null) {
                mNotificationManager.notify(getId().hashCode(), notification);
            }

            setForegroundAsync(new ForegroundInfo(getId().hashCode(), notification));
        }
    }

    private Notification createNotification(@NonNull String title, int progress, int index, int maxIndex) {
        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                InitApplication.STORAGE_CHANNEL_ID);


        PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent main = new Intent(getApplicationContext(), MainActivity.class);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                intent).build();

        builder.setContentTitle(title)
                .setSubText("" + index + "/" + maxIndex)
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(action)
                .setGroup(InitApplication.STORAGE_GROUP_ID)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setOngoing(true);

        return builder.build();
    }


    @NonNull
    @Override
    public Result doWork() {

        String uri = getInputData().getString(Content.URI);
        Objects.requireNonNull(uri);
        long root = getInputData().getLong(Content.IDX, 0L);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + uri);


        try {

            DocumentFile rootDocFile = DocumentFile.fromTreeUri(getApplicationContext(),
                    Uri.parse(uri));
            Objects.requireNonNull(rootDocFile);

            int length = rootDocFile.listFiles().length;

            boolean hasChildren = length > 0;

            String name = rootDocFile.getName();
            Objects.requireNonNull(name);


            if (hasChildren) {
                reportProgress(name, 0, 0, length);
            }

            long parent = createDir(root, name);
            try {

                threads.setThreadWork(parent, getId());
                threads.setThreadUri(parent, uri);
                threads.setThreadLeaching(parent);

                copyDir(parent, rootDocFile);

            } finally {
                threads.setThreadDone(parent);
                threads.resetThreadWork(parent);
                closeNotification();
            }


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            EVENTS.getInstance(getApplicationContext()).refresh();
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

    private long createDir(long parent, @NonNull String name) {
        long idx = docs.createDocument(parent, MimeTypeService.DIR_MIME_TYPE,
                Objects.requireNonNull(ipfs.createEmptyDir()).String(),
                null, name, 0L, false);
        docs.finishDocument(idx);
        return idx;
    }

    private void copyDir(long parent, @NonNull DocumentFile file) {

        DocumentFile[] filesInDir = file.listFiles();
        int maxIndex = filesInDir.length;
        int index = 0;
        for (DocumentFile docFile : filesInDir) {

            if (!isStopped()) {
                index++;
                if (docFile.isDirectory()) {
                    String name = docFile.getName();
                    Objects.requireNonNull(name);
                    long child = createDir(parent, name);
                    copyDir(child, docFile);
                    threads.setThreadDone(child);
                    docs.finishDocument(child);
                } else {
                    long child = copyFile(parent, docFile, index, maxIndex);
                    docs.finishDocument(child);
                }
            }
        }
    }

    private long copyFile(long parent, @NonNull DocumentFile file, int index, int maxIndex) {

        if (isStopped()) {
            return 0L;
        }

        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        THREADS threads = THREADS.getInstance(getApplicationContext());

        long idx = createThread(parent, file);
        long size = file.length();
        String name = file.getName();
        Objects.requireNonNull(name);

        Uri uri = file.getUri();
        AtomicLong refresh = new AtomicLong(System.currentTimeMillis());
        try (InputStream is = getApplicationContext().getContentResolver().openInputStream(uri)) {
            Objects.requireNonNull(is);

            Cid cid = ipfs.storeInputStream(is, new Progress() {


                @Override
                public void setProgress(int percent) {
                    reportProgress(name, percent, index, maxIndex);
                    threads.setThreadProgress(idx, percent);
                }

                @Override
                public boolean doProgress() {
                    long time = System.currentTimeMillis();
                    long diff = time - refresh.get();
                    boolean doProgress = (diff > Settings.REFRESH);
                    if (doProgress) {
                        refresh.set(time);
                    }
                    return doProgress;
                }

                @Override
                public boolean isClosed() {
                    return isStopped();
                }


            }, size);


            threads.setThreadDone(idx, cid.String());
            return idx;

        } catch (Throwable throwable) {
            threads.setThreadsDeleting(idx);
            LogUtils.error(TAG, throwable);
        }

        return 0L;
    }

    private long createThread(long parent, @NonNull DocumentFile file) {

        Uri uri = file.getUri();
        long size = file.length();
        String name = file.getName();
        Objects.requireNonNull(name);
        String mimeType = file.getType();


        return docs.createDocument(parent, mimeType, null, uri, name,
                size, false);
    }

}
