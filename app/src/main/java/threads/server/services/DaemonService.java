package threads.server.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.net.InetAddress;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.server.InitApplication;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.Content;

public class DaemonService extends Service {

    private static final String TAG = DaemonService.class.getSimpleName();
    private ConnectivityManager.NetworkCallback networkCallback;
    private NsdManager mNsdManager;

    public static void start(@NonNull Context context) {

        try {
            Intent intent = new Intent(context, DaemonService.class);
            intent.putExtra(Content.REFRESH, true);
            ContextCompat.startForegroundService(context, intent);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    public void unRegisterNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            connectivityManager.unregisterNetworkCallback(networkCallback);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void registerNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            networkCallback = new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(Network network) {
                    try {
                        ConnectivityManager connectivityManager =
                                (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

                        LinkProperties linkProperties = connectivityManager.getLinkProperties(network);
                        String interfaceName = null;
                        if (linkProperties != null) {
                            interfaceName = linkProperties.getInterfaceName();
                        }

                        IPFS ipfs = IPFS.getInstance(getApplicationContext());
                        if (interfaceName != null) {
                            ipfs.updateNetwork();
                        }
                        for (Multiaddr ma:ipfs.listenAddresses()) {
                            LogUtils.error(TAG, ma.toString());
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }

                @Override
                public void onLost(Network network) {
                }
            };


            connectivityManager.registerDefaultNetworkCallback(networkCallback);
        } catch (Exception e) {
            LogUtils.error(TAG, e);
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {

            if (intent.getBooleanExtra(Content.REFRESH, false)) {
                IPFS ipfs = IPFS.getInstance(getApplicationContext());

                Notification.Builder builder = new Notification.Builder(
                        getApplicationContext(), InitApplication.DAEMON_CHANNEL_ID);

                Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
                int viewID = (int) System.currentTimeMillis();
                PendingIntent viewIntent = PendingIntent.getActivity(getApplicationContext(),
                        viewID, notifyIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);


                Intent stopIntent = new Intent(getApplicationContext(), DaemonService.class);
                stopIntent.putExtra(Content.REFRESH, false);
                int requestID = (int) System.currentTimeMillis();
                PendingIntent stopPendingIntent = PendingIntent.getService(
                        getApplicationContext(), requestID, stopIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                String cancel = getApplicationContext().getString(android.R.string.cancel);
                Notification.Action action = new Notification.Action.Builder(
                        Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                        stopPendingIntent).build();
                builder.setSmallIcon(R.drawable.access_point_network);
                builder.addAction(action);
                builder.setOnlyAlertOnce(true);
                String port = String.valueOf(ipfs.getPort());
                builder.setContentText(getString(R.string.service_is_running, port));
                builder.setContentIntent(viewIntent);
                builder.setSubText(getApplicationContext().getString(
                        R.string.port) + " " + ipfs.getPort());
                builder.setGroup(InitApplication.DAEMON_GROUP_ID);
                builder.setCategory(Notification.CATEGORY_SERVICE);


                Notification notification = builder.build();
                startForeground(TAG.hashCode(), notification);

            } else {
                try {
                    stopForeground(true);
                } finally {
                    stopSelf();
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return START_NOT_STICKY;
    }

    private void registerService() {
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            String peerID = ipfs.getPeerID().toBase58();

            String serviceType = "_p2p._udp";
            NsdServiceInfo serviceInfo = new NsdServiceInfo();
            try {
                String address = ipfs.defaultListenAddress().toString();
                LogUtils.debug(TAG, address);
                serviceInfo.setAttribute(Protocol.Type.DNSADDR.name, address);
            } catch (Throwable throwable){
                LogUtils.error(TAG, throwable);
            }
            serviceInfo.setServiceName(peerID);
            serviceInfo.setServiceType(serviceType);
            serviceInfo.setPort(ipfs.getPort());
            mNsdManager = (NsdManager) getSystemService(Context.NSD_SERVICE);
            Objects.requireNonNull(mNsdManager);
            mNsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD,
                    RegistrationService.getInstance());

            DiscoveryService discovery = DiscoveryService.getInstance();
            discovery.setOnServiceFoundListener((info) -> mNsdManager.resolveService(info, new NsdManager.ResolveListener() {

                @Override
                public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                    LogUtils.debug(TAG, "failed " + serviceInfo.toString());
                }

                @Override
                public void onServiceResolved(NsdServiceInfo serviceInfo) {

                    try {
                        String serviceName = serviceInfo.getServiceName();
                        boolean connect = !Objects.equals(peerID, serviceName);
                        if (connect) {
                            InetAddress inetAddress = serviceInfo.getHost();

                            byte[] data = serviceInfo.getAttributes().get(
                                    Protocol.Type.DNSADDR.name);
                            if(data != null){
                                Multiaddr multiaddr = new Multiaddr(new String(data));
                                LocalConnectService.connect(getApplicationContext(),
                                        PeerId.fromBase58(
                                                multiaddr.getStringComponent(Protocol.Type.P2P)),
                                        inetAddress, serviceInfo.getPort());
                            } else {
                                LocalConnectService.connect(getApplicationContext(),
                                        PeerId.fromBase58(serviceName),
                                        inetAddress, serviceInfo.getPort());
                            }
                        }
                    } catch (Throwable e) {
                        LogUtils.error(TAG, e);
                    }
                }
            }));
            mNsdManager.discoverServices(serviceType, NsdManager.PROTOCOL_DNS_SD, discovery);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    private void unRegisterService() {
        try {
            if (mNsdManager != null) {
                mNsdManager.unregisterService(RegistrationService.getInstance());
                mNsdManager.stopServiceDiscovery(DiscoveryService.getInstance());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unRegisterNetworkCallback();
            unRegisterService();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            registerNetworkCallback();
            registerService();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

}
